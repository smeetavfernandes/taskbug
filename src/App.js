import React, { Component } from 'react';
import './App.css';
import './index.css';
import Header from './components/Header';
import TaskForm from './components/TaskForm';
import Dashboard from './components/Dashboard';
import ModalTaskForm from './components/ModalTaskForm';

class App extends Component {
  state ={
    tasks:[
      {id:1,title:"Code a react app", completed:false,category:"In Progress"},
      {id:2,title:"Code a react app2", completed:false,category:"In Progress"},
      {id:3,title:"Code a react app3", completed:false,category:"To Do"},
      {id:4,title:"Code a react app4", completed:false,category:"To Do"},
      {id:5,title:"Code a react app5", completed:false,category:"Completed"},
      {id:6,title:"Code a react app6", completed:false,category:"To Do"},
      {id:7,title:"Code a react app7", completed:false,category:"Completed"},
      {id:8,title:"Code a react app8", completed:false,category:"To Do"},
      {id:9,title:"Code a react app9", completed:false,category:"In Progress"},
      {id:10,title:"Code a react app10", completed:false,category:"To Do"}
    ],
    categories:["In Progress","To Do","Completed"],
    isOpen: false,
    //editTask:{},
    editTask:{
            title:"",
            completed:false,
            description:"",
            category:"To Do"
        }
  }

  componentDidMount(){
    var tasks= this.state.tasks;
    for(let i=0;i<tasks.length;i++)
      {
        tasks[i].description="some random text "+i;
      }
      this.setState({      
      tasks:tasks, isOpen: false
    })
  }
  onTaskSubmit=(task)=>{
    task.id=this.state.tasks.length+1;
    this.setState({
      ... this.state,
      tasks:[... this.state.tasks,task],isOpen: false
    })
  }
  onTaskUpdate=(t)=>{
    var tasks=this.state.tasks.filter((task)=>{
      if(task.id ===t.id){
      task.title=t.title;
      task.description=t.description;
      }
      return task;
    });
    this.setState({      
      tasks:tasks, isOpen: false,editTask:{
        id:"",title:"", completed:false,category:"To Do",descrption:""
      }
    })
  }
  onToggle=(id)=>{
    var tasks=this.state.tasks.filter((task)=>{
      if(task.id ===id)
        {
          task.completed=!task.completed;
        }
         return task;
        }
      );
    this.setState({      
      tasks:tasks, isOpen: false
    })
  }
  onDeleteTask=(id)=>{
  var tasks=this.state.tasks.filter((task)=>{return task.id !==id;});
    this.setState({      
      tasks:tasks, isOpen: false
    })
  }
  onEditToggle=(id)=>{
    var tasks=this.state.tasks.filter((task)=>{
      if(task.id ===id)
        {
          task.edit=!task.edit;
        }
         return task;
        }
      );
    this.setState({      
      tasks:tasks, isOpen: false
    })
  }
  onEditTaskDescChange=(id,val)=>{
      var tasks=this.state.tasks.filter((task)=>{
      if(task.id ===id)
        {
          task.description=val;
        }
         return task;
        }
      );
    this.setState({      
      tasks:tasks, isOpen: false
    })
    }
  onEditTaskChange=(id,field)=>{
      var tasks=this.state.tasks.filter((task)=>{
      if(task.id ===id)
        {
          task.title=field.value;
        }
         return task;
        }
      );
    this.setState({      
      tasks:tasks, isOpen: false
    })
  }
//onClick={this.onTogglePopup.bind(this)}
  onTogglePopup=(eTask)=> { 
   this.setState({
      ... this.state,
      isOpen: !this.state.isOpen,
      editTask:eTask
    })
  }

 onDragover=(ev)=> {
    ev.preventDefault();
    //console.log("onDragover="+ev);
}

onDrag=(ev,id)=> {
    ev.dataTransfer.setData("id", id);
    //console.log("onDrag="+id);
}

onDrop=(ev,cat)=> {
    ev.preventDefault();
    //console.log("onDrag="+cat);
    var id = parseInt(ev.dataTransfer.getData("id"));
    var tasks=this.state.tasks.filter((task)=>{
      //console.log(task.id+"==="+id);
      if(task.id===id)
        {
          task.category=cat;
        }
         return task;
        }
      );
    this.setState({      
      tasks:tasks, isOpen: false
    })
}
  render() {
    return (
      <div className="App">
        <Header/>
        <TaskForm onSubmit={this.onTaskSubmit} task={this.state.editTask}/>
        <Dashboard categories={this.state.categories} tasks={this.state.tasks} onDeleteTask={this.onDeleteTask} onToggle={this.onToggle} 
        onEditToggle={this.onEditToggle} onEditTaskChange={this.onEditTaskChange} onEditTaskDescChange={this.onEditTaskDescChange}
        onTogglePopup={this.onTogglePopup} onDragover={this.onDragover} onDrag={this.onDrag} onDrop={this.onDrop}/>  
        <ModalTaskForm show={this.state.isOpen} task={this.state.editTask}
          onClose={this.onTogglePopup} onSubmit={this.onTaskUpdate}> 
          Here's some content for the modal
        </ModalTaskForm>      
      </div>
    );
  }
}

export default App;
