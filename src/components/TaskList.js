import React from 'react';
import Task from './Task';

//export default class TaskList extends React.Component
var taskList=(props)=>
{
    //key={<props className="category"></props>}
    var taskList =props.tasks.map((task)=>{
        
        return (
            <Task {... props} task={task} />        
        )
    });
    return (
        <div key={props.category}>
        <ul className="task-list" key={props.category}>{taskList}</ul>
        </div>
        
    );
}

export default taskList;