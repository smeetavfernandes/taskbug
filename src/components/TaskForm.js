import React from 'react';
export default class TaskForm extends React.Component
{
    constructor(props)
    {
        super(props);
        this.task=props.task;
    }
    state={
        task:this.props.task,
        task1:{
            title:"",
            completed:false,
            description:"",
            category:"To Do"
        }
    }
    onTitleChange=(e)=>{
        this.setState({
            ... this.state,
            task:{
                ... this.state.task,
                title:e.target.value,
                completed:false
            }
        })
    }
    onDescChange=(e)=>{
        this.setState({
            ... this.state,
            task:{
                ... this.state.task,
                description:e.target.value
            }
        })
    }
    onSubmit=(e)=>{
        e.preventDefault();
        var {onSubmit}=this.props;
        onSubmit(this.state.task);  
          this.setState({
            ... this.state,
            task:{
                title:"",
                completed:false,
                description:""
            }
        })  
        this.taskInput.focus();  
    }
    render() {
        var {title,description}=this.state.task;
        return(
            <React.Fragment>
        <h2>Task Form</h2>
        <form onSubmit={this.onSubmit}>
        <input type="text" ref={(taskInput)=>this.taskInput=taskInput} 
        placeholder="Enter task" value={title} onChange={this.onTitleChange}/>
<br/>
        <textarea ref={(taskDescInput)=>this.taskDescInput=taskDescInput} 
         placeholder="Enter description" onChange={this.onDescChange} value={description}/>
<br/>
        <input type="submit" value="Submit"/>
        </form>
        </React.Fragment>
    );
    }
}